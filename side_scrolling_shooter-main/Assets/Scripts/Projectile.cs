﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int signo;
    virtual public void Update()
    {

    }

    internal virtual void OnCollisionEnter(Collision collision)// destrulle el proyectil cuando choca con una pared
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Floor"))
        {
            Destroy(this.gameObject);
        }
    }
}

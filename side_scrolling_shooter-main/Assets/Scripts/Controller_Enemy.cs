﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public float enemySpeed;

    public GameObject enemyProjectile;

    public GameObject powerUp;
    public int valor;

    public float tempMax, tempMin;
    float shootingCooldown = 2.5f;

    void Start()
    {
        shootingCooldown = UnityEngine.Random.Range(tempMin, tempMax);
    }

    public virtual void Update()
    {
        shootingCooldown -= Time.deltaTime;
        ShootPlayer();
    }

    void ShootPlayer()// dispara cada sieto tiempo
    {
        if (shootingCooldown < 0)
        {
            Instantiate(enemyProjectile, transform.position, Quaternion.identity);
            shootingCooldown = UnityEngine.Random.Range(tempMin, tempMax);
        }
    }

    internal virtual void OnCollisionEnter(Collision collision)// se destruye a si mismo y el proyectil ademas de aumentar los puntos del jugador corespondiente
    {
        if ( gameObject.tag == "Enemy")
        {
            if (collision.gameObject.CompareTag("Jugador 1"))
            {
                GeneratePowerUp();
                Destroy(collision.gameObject);
                Destroy(this.gameObject);
                Controller_Hud.puntosJugadorUno += valor;
                Controller_Instantiator.enemigos--;
            }
            if (collision.gameObject.CompareTag("Jugador 2"))
            {
                GeneratePowerUp();
                Destroy(collision.gameObject);
                Destroy(this.gameObject);
                Controller_Hud.puntosJugadorDos += valor;
                Controller_Instantiator.enemigos--;
            }
        }
    }

    private void GeneratePowerUp()// genera un objeto de power up (se llama antes de destruirse a si mismo)
    {
        int rnd = UnityEngine.Random.Range(0, 3);
        if (rnd == 2)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity);
        }
    }
}

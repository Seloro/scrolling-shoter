﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

public class Controller_Projectile : Projectile
{
    public float projectileSpeed;
    public Rigidbody rb;

    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    
    public override void Update()
    {
        ProjectileDirection();
        base.Update();
    }

    public virtual void ProjectileDirection()// mueve hacia la derecha la bala teniendo en cuenta su rotacion
    {
        rb.velocity = transform.right * projectileSpeed * signo;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigzagEnemy : Controller_Enemy
{
    private Rigidbody rb;
    int x, y;
    float temp;

    void Start()
    {
        temp = UnityEngine.Random.Range(2.5f, 5);
        rb = GetComponent<Rigidbody>();
        x = UnityEngine.Random.Range(-1, 2);
        y = UnityEngine.Random.Range(-1, 2);
    }

    void FixedUpdate()
    {
        temp -= Time.deltaTime;
        rb.AddForce(new Vector3(x, y, 0) * enemySpeed);
    }
    public void cambio()// cambia la direccion en la que se mueve x, y cada sierto tiempo
    {
        if (temp < 0)
        {
            temp = UnityEngine.Random.Range(2.5f, 5);
            x = UnityEngine.Random.Range(-1, 2);
            y = UnityEngine.Random.Range(-1, 2);
        }
        if (x == 0 && y == 0)// si ambas direcciones resultan tener un valor de 0 (es desir no hay movimiento) se vuelve a asignar un valor random
        {
            x = UnityEngine.Random.Range(-1, 2);
            y = UnityEngine.Random.Range(-1, 2);
        }
    }
    internal override void OnCollisionEnter(Collision collision)// cambia a la direccion opuesta cuando choca con una pared o enemigo (enemigo en modo escudo incluido)
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Escudo"))
        {
            x *= -1;
            y *= -1;
        }
        base.OnCollisionEnter(collision);
    }
    public override void Update()
    {
        cambio();
        base.Update();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUp : MonoBehaviour
{
    private Rigidbody rb;
    float temp, x, y, velocidad;
    bool rojo;
    float vida = 10;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (UnityEngine.Random.Range(0, 2) == 1)// elije un color inicial random
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
            rojo = true;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.blue;
        }
        x = UnityEngine.Random.Range(-1, 2);
        y = UnityEngine.Random.Range(-1, 2);
        velocidad = UnityEngine.Random.Range(1, 3);
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector3(x, y, 0) * velocidad;
    }
    private void Update()
    {
        condicionesTemporales();
        muerte();
    }
    private void cambiColor()// cambia el color cada sierto tiempo
    {
        if (rojo)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
        rojo = !rojo;
    }
    private void cambioDireccion()// cambia la direccion x,y en la que se mueve de forma aleatoria
    {
        x = UnityEngine.Random.Range(-1, 2);
        y = UnityEngine.Random.Range(-1, 2);
        velocidad = UnityEngine.Random.Range(1, 3);
    }
    private void condicionesTemporales()// ejecta los camvios cuando pasa el tiempo esperado
    {
        temp += Time.deltaTime;
        if (temp > .5f)
        {
            cambiColor();
            cambioDireccion();
            temp = 0;
        }
    }
    private void OnCollisionEnter(Collision collision)// camvia a la dierccion opuesta al colisionar con algo, si ese algo es una bala jugador les abilita el power up
    {
        x *= -1;
        y *= -1;
        if (collision.gameObject.CompareTag("Jugador 1"))
        {
            Controller_Player.powerUpUno = true;
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Jugador 2"))
        {
            Controller_Player.powerUpDos = true;
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
    private void muerte()// destrulle el objeto despues de un tiempo
    {
        vida -= Time.deltaTime;
        if (vida < 0)
        {
            Destroy(gameObject);
        }
    }
}

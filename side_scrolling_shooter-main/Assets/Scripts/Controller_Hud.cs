﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;

    public static bool gameOver;

    public Text pointsTextUno, pointsTextDos, jugadorUnoText, jugadorDosText, tempText;

    private Controller_Player jugadorUno, jugadorDos;
    static public int puntosJugadorUno, puntosJugadorDos;
    float temp = 300;

    void Start()
    {
        gameOver = false;
        gameOverText.gameObject.SetActive(false);
        jugadorUno = Controller_Player.jugadorUno;
        jugadorDos = Controller_Player.jugadorDos;
    }

    void Update()
    {
        if (jugadorUno.grado < 0 ||jugadorDos.grado < 0 || temp < 0)
        {
            gameOver = true;
            if (jugadorUno.grado < 0)
            {
                puntosJugadorDos += 10000;
            }
            else if (jugadorDos.grado < 0)
            {
                puntosJugadorUno += 10000;
            }
        }
        if (gameOver)// para el juego cuando termina
        {
            Time.timeScale = 0;
            if (puntosJugadorUno == puntosJugadorDos)// si hay enpate
            {
                gameOverText.text = "Enpate";
            }
            else// si uno de los jugadors gana
            {
                if (puntosJugadorUno > puntosJugadorDos)
                {
                    gameOverText.text = "Gana jugador 1";
                }
                else
                {
                    gameOverText.text = "Gana jugador 2";
                }
            }
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            pointsTextUno.text = "Score: " + puntosJugadorUno.ToString();
            pointsTextDos.text = "Score: " + puntosJugadorDos.ToString();
        }
        jugadorUnoText.text = "Jugador 1 grado: " + (jugadorUno.grado + 1) + " vida: " + jugadorUno.vida;
        jugadorDosText.text = "Jugador 2 grado: " + (jugadorDos.grado + 1) + " vida: " + jugadorDos.vida;
        tempText.text = "Tiempo restante: " + ((int)temp).ToString();
        temp -= Time.deltaTime;
    }
}

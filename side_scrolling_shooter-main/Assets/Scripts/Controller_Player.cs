﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public bool soyJugadorDos;
    public float speed, velocidadBala, recarga;
    private Rigidbody rb;
    public GameObject projectile;
    public GameObject laser;
    public GameObject mira;
    public int grado;
    internal bool doubleShootUno, doubleShootDos;
    internal float shootingCount;
    internal bool laserOn;
    public static Controller_Player jugadorUno, jugadorDos;
    public int vida = 3;
    public static bool powerUpUno, powerUpDos;
    static public float velocidadBalaJugadorUno, velocidadBalaJugadorDos;
    bool gradoUno, gradoCuatro, laserCreado, dañoLaser, inmunidad;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        laserOn = false;
        if (soyJugadorDos)
        {
            jugadorDos = this;
        }
        else
        {
            jugadorUno = this;
        }
        recarga = 1;
    }

    private void Update()
    {
        if (soyJugadorDos)// ejecuta si es el jugador dos
        {
            disparoDos();
            if (powerUpDos && grado != 5)
            {
                grado++;
                powerUpDos = false;
            }
            velocidadBalaJugadorDos = velocidadBala;
        }
        else// ejecuta si es el jugador uno
        {
            disparoUno();
            if (powerUpUno && grado != 5)
            {
                grado++;
                powerUpUno = false;
            }
            velocidadBalaJugadorUno = velocidadBala;
        }
        powerUp();
        trucos();
        if (dañoLaser)// vierde vida si esta dentro del laser enemigo
        {
            vida--;
        }
        if (vida < 0 && !inmunidad)// pierde un grado y recupera vida si su vida es menor a 0
        {
            grado--;
            vida = 3;
        }
    }

    public virtual void FixedUpdate()
    {
        if (soyJugadorDos)
        {
            movimientoDos();
        }
        else
        {
            movimientoUno();
        }
        
    }

    public virtual void disparoUno()// dispara jugador uno, si no tiene el power up laser dispara balas o una bala normal
    {
        shootingCount -= Time.deltaTime;
        if (Input.GetKey(KeyCode.Space) && shootingCount<0)
        {
            if (laserOn && !laserCreado)
            {
                Instantiate(laser, mira.transform.position, Quaternion.identity).transform.parent = mira.transform;
                laserCreado = true;
            }
            else if (!laserCreado)
            {
                Instantiate(projectile, mira.transform.position, Quaternion.identity);
                shootingCount = recarga;
                if (doubleShootUno)
                {
                    Instantiate(projectile, mira.transform.position, Quaternion.Euler(0, 0, 10));
                }
                if (doubleShootDos)
                {
                    Instantiate(projectile, mira.transform.position, Quaternion.Euler(0, 0, -10));
                }
            }
        }
    }
    public virtual void disparoDos()// disparo para jugador dos, igual que el uno pero se acciona con el joystick
    {
        shootingCount -= Time.deltaTime;
        if (Input.GetKey(KeyCode.Joystick1Button0) && shootingCount < 0)
        {
            if (laserOn && !laserCreado)
            {
                Instantiate(laser, mira.transform.position, Quaternion.identity).transform.parent = mira.transform;
                laserCreado = true;
            }
            else if (!laserCreado)
            {
                Instantiate(projectile, mira.transform.position, Quaternion.identity);
                shootingCount = recarga;
                if (doubleShootUno)
                {
                    Instantiate(projectile, mira.transform.position, Quaternion.Euler(0, 0, -10));
                }
                if (doubleShootDos)
                {
                    Instantiate(projectile, mira.transform.position, Quaternion.Euler(0, 0, 10));
                }
            }
        }

    }

    private void movimientoUno()// mueve al jugador uno
    {
        if (Input.GetKey(KeyCode.W))
        {
            rb.velocity += Vector3.up * speed * Time.deltaTime;
        }
        else if ((Input.GetKey(KeyCode.S)))
        {
            rb.velocity -= Vector3.up * speed * Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector3 (rb.velocity.x, 0, rb.velocity.z);
        }
        if ((Input.GetKey(KeyCode.D)))
        {
            rb.velocity += Vector3.right * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity -= Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
        }
    }
    private void movimientoDos()// mueve al jugador dos
    {
        if (Input.GetAxis("Vertical") > 0)
        {
            rb.velocity += Vector3.up * speed * Time.deltaTime;
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            rb.velocity -= Vector3.up * speed * Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            rb.velocity += Vector3.right * speed * Time.deltaTime;
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            rb.velocity -= Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemyProjectile"))// si le pega una bala de un enemigo pierde vida
        {
            Destroy(collision.gameObject);
            vida--;
        }
        if (soyJugadorDos)// si es jugador dos y resive una bala del otro jugador pierde vide y le da puntos al otro jugador
        {
            if (collision.gameObject.CompareTag("Jugador 1"))
            {
                Destroy(collision.gameObject);
                vida--;
                Controller_Hud.puntosJugadorUno += 4;
            }
        }
        else// si es jugador uno y resive una bala del otro jugador pierde vide y le da puntos al otro jugador
        {
            if (collision.gameObject.CompareTag("Jugador 2"))
            {
                Destroy(collision.gameObject);
                vida--;
                Controller_Hud.puntosJugadorDos += 4;
            }
        }
    }
    private void powerUp()// adquiere un nuevo poder o lo pierde dependiendo de su grado
    {
        if (grado == 1 && !gradoUno)
        {
            speed *= 2;
            gradoUno = !gradoUno;
        }
        else if (grado < 1 && gradoUno)
        {
            speed /= 2;
            gradoUno = !gradoUno;
        }
        if (grado == 4 && !gradoCuatro)
        {
            velocidadBala *= 2;
            recarga /= 2;
            gradoCuatro = !gradoCuatro;
        }
        else if (grado < 4 && gradoCuatro)
        {
            velocidadBala /= 2;
            recarga *= 2;
            gradoCuatro = !gradoCuatro;
        }
        if (grado == 2)
        {
            doubleShootUno = true;
        }
        else if (grado < 2)
        {
            doubleShootUno = false;
        }
        if (grado == 3)
        {
            doubleShootDos = true;
        }
        else if (grado < 3)
        {
            doubleShootDos = false;
        }
        if (grado == 5)
        {
            laserOn = true;
        }
        else if (grado < 5)
        {
            laserOn = false;
        }
    }
    private void trucos()// trucos para umentar el grado o acerse inmortal
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            grado++;
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            inmunidad = !inmunidad;
        }
    }
    private void OnTriggerEnter(Collider other)// detecta cuando se entro en el laser enemigo
    {
        if (soyJugadorDos)
        {
            if (other.gameObject.CompareTag("Jugador 1"))
            {
                dañoLaser = true;
            }
        }
        else
        {
            if (other.gameObject.CompareTag("Jugador 2"))
            {
                dañoLaser = true;
            }
        }
    }
    private void OnTriggerExit(Collider other)// detecta cuando se sale del laser enemigo
    {
        if (soyJugadorDos)
        {
            if (other.gameObject.CompareTag("Jugador 1"))
            {
                dañoLaser = false;
            }
        }
        else
        {
            if (other.gameObject.CompareTag("Jugador 2"))
            {
                dañoLaser = false;
            }
        }
    }
}

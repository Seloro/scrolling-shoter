﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Instantiator : MonoBehaviour
{
    public float temp;
    public  List<GameObject> enemies;
    public GameObject instantiatePos;
    static public int enemigos;

    void Start()
    {
        temp = UnityEngine.Random.Range(2.5f, 5);
    }

    void Update()
    {
        if (enemigos < 5)
        {
            temp -= Time.deltaTime;
        }
        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        if (temp <= 0)// intancia enemigos cada sierto tiempo y si los enemigos son menos de 5
        {
            float x = instantiatePos.transform.position.x + UnityEngine.Random.Range(-10, 11);
            float y = instantiatePos.transform.position.y + UnityEngine.Random.Range(-18, 19);
            int rnd = UnityEngine.Random.Range(0, enemies.Count);
            Vector3 transform = new Vector3(x, y, 0);
            Instantiate(enemies[rnd], transform, Quaternion.identity);
            enemigos++;
            temp = UnityEngine.Random.Range(2.5f, 5);
        }
    }
}

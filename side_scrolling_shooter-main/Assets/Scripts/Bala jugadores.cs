using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balajugadores : Controller_Projectile
{
    private void Start()
    {
        if (gameObject.CompareTag("Jugador 1"))// medifica su velocidad dependendiendo si se tiene el powerup corespondiente
        {
            projectileSpeed = Controller_Player.velocidadBalaJugadorUno;
        }
        else
        {
            projectileSpeed = Controller_Player.velocidadBalaJugadorDos;
        }
    }
    internal override void OnCollisionEnter(Collision collision)// cambia la direccion de la bala si coliciona con un enemigo en escudo
    {
        if (collision.gameObject.CompareTag("Escudo"))
        {
            if (gameObject.CompareTag("Jugador 1"))
            {
                gameObject.tag = "Jugador 2";
            }
            else
            {
                gameObject.tag = "Jugador 1";
            }
            signo *= -1;
        }
        base.OnCollisionEnter(collision);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)// destrulle los objetos que entran en contaco por evento triger
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("EnemyProjectile") || other.gameObject.CompareTag("Jugador 1") || other.gameObject.CompareTag("Jugador 2"))
        {
            Destroy(other.gameObject);
        }
    }
}

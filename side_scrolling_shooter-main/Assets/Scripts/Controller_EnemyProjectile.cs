﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_EnemyProjectile : Projectile
{
    private Vector3 direction;

    private Rigidbody rb;

    public float enemyProjectileSpeed;

    void Start()// elije un jugador si mueve la vala hasta su direccion (la posicion del jugador elejino en el momento en el que se ejecuta el start)
    {
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            direction = -(this.transform.localPosition - Controller_Player.jugadorUno.gameObject.transform.localPosition).normalized;
        }
        else
        {
            direction = -(this.transform.localPosition - Controller_Player.jugadorDos.gameObject.transform.localPosition).normalized;
        }
        rb = GetComponent<Rigidbody>();
    }

    
    public override void Update()
    {
        rb.AddForce(direction*enemyProjectileSpeed);
        base.Update();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_escudo : Controller_Enemy
{
    float temp;
    bool escudo;
    Color colorOriginal;
    private void Start()
    {
        colorOriginal = gameObject.GetComponent<Renderer>().material.color;
        if (UnityEngine.Random.Range(0, 2) == 1)
        {
            escudo = true;
            temp = UnityEngine.Random.Range(5, 11);
        }
        else
        {
            temp = UnityEngine.Random.Range(1.5f, 2.5f);
        }
    }

    public override void Update()// camcia su etiqueta y color cada sierto tiempo
    {
        temp -= Time.deltaTime;
        if (temp < 0)
        {
            escudo = !escudo;
            if (escudo)
            {
                temp = UnityEngine.Random.Range(5, 11);
            }
            else
            {
                temp = UnityEngine.Random.Range(1.5f, 2.5f);
            }
        }
        if (escudo)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.gray;
            gameObject.tag = "Escudo";
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = colorOriginal;
            gameObject.tag = "Enemy";
        }
        base.Update();
    }
}

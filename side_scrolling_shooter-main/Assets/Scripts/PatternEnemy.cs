﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternEnemy : Controller_Enemy
{
    private Rigidbody rb;
    int x, y;
    float xLimiteSuperior, xLimiteInferior, yLimiteSuperior, yLimiteInferior;
    float temp;

    void Start()
    {
        temp = UnityEngine.Random.Range(2f, 5f);
        rb = GetComponent<Rigidbody>();
        x = UnityEngine.Random.Range(-1, 2);
        y = UnityEngine.Random.Range(-1, 2);
        xLimiteInferior = transform.position.x - UnityEngine.Random.Range(5, 11);
        xLimiteSuperior = transform.position.x + UnityEngine.Random.Range(5, 11);
        yLimiteInferior = transform.position.y - UnityEngine.Random.Range(5, 11);
        yLimiteSuperior = transform.position.y + UnityEngine.Random.Range(5, 11);// establese limites (un cuadrado de cordenadas)
    }

    override public void Update()
    {
        temp -= Time.deltaTime;
        if (x == 0 && y == 0)// si los valores de x e y son 0 vuelve a lejer valores random
        {
            x = UnityEngine.Random.Range(-1, 2);
            y = UnityEngine.Random.Range(-1, 2);
        }
        cambiDireccion();
        base.Update();
    }
    private void cambiDireccion()// cambia la direccion si se pasa de los limites establecidos
    {
        if (transform.position.x < xLimiteInferior)
        {
            x = 1;
        }
        if (transform.position.x > xLimiteSuperior)
        {
            x = -1;
        }
        if (transform.position.y < yLimiteInferior)
        {
            y = 1;
        }
        if (transform.position.y > yLimiteSuperior)
        {
            y = -1;
        }
        if (temp < 0)
        {
            x = UnityEngine.Random.Range(-1, 2);
            y = UnityEngine.Random.Range(-1, 2);
            temp = UnityEngine.Random.Range(2f, 5f);
        }
    }
    internal override void OnCollisionEnter(Collision collision)// cambia a la direccion opuesta si choca con una pared
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            x *= -1;
            y *= -1;
        }
        base.OnCollisionEnter(collision);
    }
    void FixedUpdate()
    {
        rb.AddForce(new Vector3(x, y, 0) * enemySpeed);
    }
}
